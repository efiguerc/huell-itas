require 'test_helper'

class UserStoriesTest < ActionDispatch::IntegrationTest

  test "visiting huell-itas page" do
    visit root_path
    assert_current_path '/'
    assert_equal "Welcome", find('h1').text
  end

  test "listing suppliers" do
    visit root_path
    click_link "Suppliers"
    assert_current_path ('/' + I18n.locale.to_s + suppliers_path.to_s)
    assert_equal "Suppliers new", find('h1').text
    assert_text suppliers(:one).nick
    assert_text suppliers(:two).nick
    assert_text suppliers(:three).nick
  end

  test "registering a new supplier" do
    visit root_path
    click_link "Suppliers"
    click_link "new"
    assert_current_path ('/' + I18n.locale.to_s + new_supplier_path.to_s)
    assert_equal "New Supplier", find('h1').text
    # fill_in I18n.t('suppliers.index.nick'), with: "pets"
    fill_in "Alias", with: "pets2"
    fill_in "Company", with: "pets inc"
    fill_in "Tax", with: "DIGJ780223C33"
    fill_in "Address 1", with: "CHAPULIN 189"
    fill_in "Address 2", with: "COL LAS PERITAS"
    fill_in "City", with: "XOCHIMILCO"
    fill_in "State", with: "DISTRITO FEDERAL"
    fill_in "Zip", with: "16010"
    fill_in "Country", with: "MÉXICO"
    fill_in "Mobile", with: "5534541292"
    fill_in "Phone", with: "56748060"
    fill_in "E-mail", with: "info@petsinc.com.mx"
    fill_in "Webpage", with: "www.petsinc.com.mx"
    fill_in "Contact", with: "DIAZ GONZALEZ JAIME"
    click_on "Save Supplier"
    assert_current_path ('/' + I18n.locale.to_s + suppliers_path.to_s)
    assert_equal "Supplier was successfully created.", find('div.alert').text
    assert_text("pets2")
  end

  test "showing a supplier" do
    visit root_path
    click_link "Suppliers"
    click_link suppliers(:two).nick
    assert_current_path ('/' + I18n.locale.to_s + supplier_path(suppliers(:two)).to_s)
    assert_equal suppliers(:two).nick, find('h1').text
    assert_text suppliers(:two).name
  end

  test "editing a supplier" do
    visit root_path
    click_link "Suppliers"
    click_link suppliers(:two).nick
    click_link "Edit"
    assert_current_path ('/' + I18n.locale.to_s + edit_supplier_path(suppliers(:two)).to_s)
    fill_in "Alias", with: "best pets"
    click_on "Save Supplier"
    assert_current_path ('/' + I18n.locale.to_s + supplier_path(suppliers(:two)).to_s)
    assert_equal "Supplier was successfully updated.", find('div.alert').text
    assert_equal "best pets", find('h1').text
  end

  test "deleting a supplier" do
    Capybara.current_driver = :selenium_firefox
    visit root_path
    click_link "Suppliers"
    click_link suppliers(:two).nick
    assert_current_path ('/' + I18n.locale.to_s + supplier_path(suppliers(:two)).to_s)
    # click_link "Delete"
    # Capybara.current_driver = :selenium
    # Capybara.current_driver = Capybara.javascript_driver # :selenium by default
    # Capybara.current_driver = Selenium::WebDriver.for :firefox, marionette: true
    # Capybara.current_driver = :selenium_firefox
    accept_confirm do
      click_link "Delete"
      # find('a', :text => "Delete").click
    end
    # Capybara.use_default_driver
    # Capybara.current_driver = :selenium
    # page.accept_alert do click_button "OK" end
    assert_current_path ('/' + I18n.locale.to_s + suppliers_path.to_s)
    assert_equal "Supplier was successfully destroyed.", find('div.alert').text
    assert_no_text suppliers(:two).nick
    Capybara.use_default_driver
  end
end

