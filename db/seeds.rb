# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Supplier.delete_all

Supplier.create!(
  nick:     "LUPI PEZ",
  name:      "",
  tax_id:    "",
  address1:  "RIO FRIO  No 176, LOC A",
  address2:  "MAGDALENA MIXHUCA",
  city:      "VENUSTIANO CARRANZA",
  state:     "DISTRITO FEDERAL",
  zip:       "15850",
  country:   "MEXICO",
  contact:   "ANA,DON OSCAR",
  mobile:    "",
  phone:     "41950120",
  email:     "",
  webpage:   ""
)

Supplier.create!(
  nick:     "RAY PET",
  name:      "ISMAEL ALVAREZ GUEVARA",
  tax_id:    "",
  address1:  "LABRADORES 82 , ESQ. IMPRENTA",
  address2:  "MORELOS",
  city:      "VENUSTIANO CARRANZA",
  state:     "DISTRITO FEDERAL",
  zip:       "1",
  country:   "MEXICO",
  contact:   "AMIGUITO",
  mobile:    "",
  phone:     "26164939",
  email:     "",
  webpage:   ""
)

Supplier.create!(
  nick:     "FANTASY PETS",
  name:      "FANTASY PETS",
  tax_id:    "",
  address1:  "PRIVADA COSNTITUCIÓN No 53",
  address2:  "EL MANTE",
  city:      "GUADALAJARA",
  state:     "JALISCO",
  zip:       "45235",
  country:   "MEXICO",
  contact:   "",
  mobile:    "",
  phone:     "(0133) 36925183",
  email:     "fantasymundo@live.com",
  webpage:   "www.fantasymundoanimal.com"
)

17.times do |n|
  nick = "#{Faker::Company.name} #{n}"
  Supplier.create!(
    nick:     nick,
    name:      "#{nick} S.A. DE C.V. #{n}",
    tax_id:    Faker::Company.duns_number,
    address1:  Faker::Address.street_address,
    address2:  Faker::Address.secondary_address,
    city:      Faker::Address.city,
    state:     Faker::Address.state,
    zip:       Faker::Number.number(5),
    country:   "MEXICO",
    contact:   Faker::Name.name,
    mobile:    Faker::PhoneNumber.cell_phone,
    phone:     Faker::PhoneNumber.phone_number,
    email:     Faker::Internet.email,
    webpage:   "#{n}#{Faker::Internet.domain_name}"
  )
end
