class CreateSuppliers < ActiveRecord::Migration
  def change
    create_table :suppliers do |t|
      t.string :nick
      t.string :name
      t.string :tax_id
      t.string :address1
      t.string :address2
      t.string :city
      t.string :state
      t.string :zip
      t.string :country
      t.string :contact
      t.string :mobile
      t.string :phone
      t.string :email
      t.string :webpage

      t.timestamps null: false
    end
  end
end
