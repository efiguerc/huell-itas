class AddKeywordsToSuppliers < ActiveRecord::Migration
  def change
    add_column :suppliers, :keywords, :text
  end
end
