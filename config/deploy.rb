# config valid only for current version of Capistrano
lock '3.5.0'

set :application, 'huell-itas'
set :repo_url, "git@bitbucket.org:efiguerc/#{fetch(:application)}.git"

# Default branch is :master
set :branch, `git branch`.match(/^\* ([0-9A-Za-z_]+)$/m)[1]

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, '/var/www/my_app_name'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: 'log/capistrano.log', color: :auto, truncate: :auto

# Default value for :pty is false
set :pty, true

# Default value for :linked_files is []
set :linked_files, fetch(:linked_files, []).push(
  'config/database.yml',
  'config/secrets.yml',
  'config/application.yml'
)

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push(
  'log',
  'tmp/pids',
  'tmp/cache',
  'tmp/sockets',
  'vendor/bundle'
)

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

set :rbenv_type, :user
set :rbenv_ruby, '2.3.1'

set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles, :all # default value

namespace :deploy do

  desc "Sets up the config to handle http app request by Nginx."
  before :starting, :setup_nginx do
    on roles(:app) do
      execute "mkdir -p #{fetch(:deploy_to)}/current/config"
      upload! 'config/nginx.conf', "#{fetch(:deploy_to)}/current/config/nginx.conf"
      sudo "ln -nfs #{fetch(:deploy_to)}/current/config/nginx.conf /etc/nginx/sites-enabled/#{fetch(:application)}"
      execute "rm -Rf #{fetch(:deploy_to)}/current"
    end
  end

  desc "Sets up the run control script to run app on Puma."
  before :setup_nginx, :setup_puma do
    on roles(:app) do
      execute "mkdir -p #{fetch(:deploy_to)}/current/config"
      upload! 'config/puma_init.sh', "#{fetch(:deploy_to)}/current/config/puma_init.sh"
      sudo "ln -nfs #{fetch(:deploy_to)}/current/config/puma_init.sh /etc/init.d/puma_#{fetch(:application)}"
      sudo "update-rc.d puma_#{fetch(:application)} defaults"
      execute "rm -Rf #{fetch(:deploy_to)}/current"
    end
  end

  desc "Sets up the secrets needed for the app to start."
  before :setup_puma, :setup_secrets do
    on roles(:app) do
      upload! 'config/database.yml', "#{shared_path}/config/database.yml"
      upload! 'config/secrets.yml', "#{shared_path}/config/secrets.yml"
      upload! 'config/application.yml', "#{shared_path}/config/application.yml"
    end
  end

  desc "Make sure local git is in sync with remote."
  before :deploy, :check_revision do
    on roles(:web) do
      unless `git status` =~ /nothing to commit/
        warn "There are changes not commited: Run `git add && git commit` to save changes."
        exit
      end
      unless `git rev-parse #{fetch(:branch)}` == `git rev-parse origin/#{fetch(:branch)}`
        warn "HEAD is not the same as origin/master: Run `git push` to sync changes."
        exit
      end
    end
  end

  desc "Make sure the puma server is started."
  after :publishing, :restart_puma do
    on roles(:app) do
      sudo "service puma_#{fetch(:application)} stop"
      sudo "service puma_#{fetch(:application)} start"
    end
  end

  desc "Make sure the nginx server is started."
  after :restart_puma, :restart_nginx do
    on roles(:app) do
      sudo "service nginx restart"
    end
  end

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      within release_path do
        execute :rake, 'cache:clear'
      end
    end
  end

end
