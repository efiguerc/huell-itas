root = "/var/www/huell-itas/current"
directory root

# Change to match your CPU core count
workers 2

# Min and Max threads per worker
threads 1, 6

worker_timeout 30

# Default to production
rails_env = ENV['RAILS_ENV'] || "production"
environment rails_env

# Set up socket location
bind "unix://#{root}/tmp/sockets/puma.sock"

# Logging
stdout_redirect "#{root}/log/puma.stdout.log", "#{root}/log/puma.stderr.log", true

# Set master PID and state locations
pidfile "#{root}/tmp/pids/puma.pid"
state_path "#{root}/tmp/pids/puma.state"
activate_control_app

on_worker_boot do
  require "active_record"
  ActiveRecord::Base.connection.disconnect! rescue ActiveRecord::ConnectionNotEstablished
  ActiveRecord::Base.establish_connection(YAML.load_file("#{root}/config/database.yml")[rails_env])
end

