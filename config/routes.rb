Rails.application.routes.draw do

  get 'static_pages/contact'

  scope '(:locale)' do
    root                     'static_pages#home'
    get       'help'      => 'static_pages#help'
    resources :suppliers
  end

end
