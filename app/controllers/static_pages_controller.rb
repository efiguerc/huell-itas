class StaticPagesController < ApplicationController
  def contact
  end

  def help
  end

  def home
  end
end
