class SuppliersController < ApplicationController
  before_action :set_supplier, only: [:show, :edit, :update, :destroy]

  def index
    @suppliers = Supplier.search(params[:keyword])
  end

  def show
  end

  def new
    @supplier = Supplier.new
  end

  def edit
  end

  def create
    @supplier = Supplier.new(supplier_params)

    if @supplier.save
      flash[:success] = "Supplier was successfully created."
      redirect_to suppliers_url
    else
      render :new
    end
  end

  def update
    if @supplier.update(supplier_params)
      flash[:success] = "Supplier was successfully updated."
      redirect_to @supplier
    else
      render :edit
    end
  end

  def destroy
    @supplier.destroy
    flash[:success] = "Supplier was successfully destroyed."
    redirect_to suppliers_url
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_supplier
      @supplier = Supplier.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def supplier_params
      params.require(:supplier).permit(:nick, :name, :tax_id, :address1, :address2, :city, :state, :zip, :country, :contact, :mobile, :phone, :email, :webpage)
    end
end
