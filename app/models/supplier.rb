class Supplier < ActiveRecord::Base
  scope :search, ->(keyword){ where('keywords LIKE ?', "%#{keyword.downcase}%") if keyword.present? }

  before_save :set_keywords

  validates :nick, presence: true,
                    uniqueness: { case_sensitive: false }

  protected

    def set_keywords
      self.keywords = [self.nick, name, tax_id, address1, address2, city, state, zip, country,
        contact, mobile, phone, email, webpage].map(&:downcase).join(' ')
    end
end
